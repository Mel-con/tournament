<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function social_redirect($provider)
    {
        // dd($provider);
        return Socialite::driver($provider)->redirect();
    }

    public function social_callback(Request $request, $provider)
    {
        if($request->has('error')){
            return redirect('/shop');
        }
        if ($provider == 'Facebook') {
            $fb_user = Socialite::with('Facebook')->fields(['id', 'name', 'email'])->user();



            $condidate = User::where('email', '=', $fb_user->email)->first();
            if ($condidate) {
                Auth::login($condidate);
                return redirect('/shop');
            } else {
                $user = new User();
                $user->fb_id = $fb_user->id;
                $user->name = $fb_user->name;
                $user->first_name = explode(" ",$fb_user->name)[0];
                $user->last_name = explode(" ",$fb_user->name)[1];
                $user->email = $fb_user->email;
                $user->role_id = 0;
                $user->verified = 1;
                if ($user->save()) {
                    Auth::login($user);
                    return redirect('/shop');
                }
            }
        }

        if ($provider == 'Google') {
            $google_user = Socialite::with('Google')->user();

            $candidate = User::where('email', '=', $google_user->email)->first();
            if ($candidate) {
                Auth::login($candidate);
                return redirect('/shop');
            } else {
                $user = new User();
                $user->google_id = $google_user->id;
                $user->email = $google_user->email;
                $user->name = $google_user->name;
                $user->first_name = $google_user->user['given_name'];
                $user->last_name = $google_user->user['family_name'];
                $user->role_id = 0;
                $user->verified = 1;
                if ($user->save()) {
                    Auth::login($user);
                    return redirect('/shop');
                }

            }
        }

    }

    protected function signout()
    {
        Auth::logout();
        return redirect()->back();
    }

}
