<?php

namespace App\Http\Controllers\Site;

use App\Games;
use App\Participant;
use App\TournamentSettings;
use App\TournamentTable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;

class GamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Games $games
     * @return \Illuminate\Http\Response
     */
    public function show(Games $games)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Games $games
     * @return \Illuminate\Http\Response
     */
    public function edit(Games $games)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Games $games
     * @return \Illuminate\Http\Response
     */

    public function initializeTable ($tournament_id) {

        $tournament_tables= TournamentTable::where('tournament_id', $tournament_id)->get();
        foreach ($tournament_tables as $tournament_table){
            $tournament_table->played_games = 0 ;
            $tournament_table->win = 0 ;
            $tournament_table->lose = 0 ;
            $tournament_table->draw = 0 ;
            $tournament_table->gol = 0 ;
            $tournament_table->gol_against = 0 ;
            $tournament_table->points = 0 ;
            $tournament_table->place = 0 ;

            $tournament_table->save();
        }

    }

    public function generateTable($tournament_id){
        $games = Games::where('tournament_id',$tournament_id)->get();
        $tournament_settings = TournamentSettings::where('tournament_id',$tournament_id)->first();


        foreach ($games as $game){

            $tournament_table_1 = TournamentTable::where('participant_id', $game->player_1)->first();
            $tournament_table_1->gol += $game->result_1;
            $tournament_table_1->gol_against += $game->result_2;


            if ($game->winner == 1){
                if ($game->technical_lose == 2 ) {
                    $tournament_table_1->points += $tournament_settings->technical_win_point;
                }elseif ($game->tiabreak == 1){
                    $tournament_table_1->points += $tournament_settings->win_point_on_tiabreak;
                }else{
                    $tournament_table_1->points += $tournament_settings->win_point;
                }
                $tournament_table_1->win += 1;
                $tournament_table_1->increment('played_games');
            }elseif($game->winner == 2){
                if ($game->technical_lose == 1){
                    $tournament_table_1->points += $tournament_settings->technical_lose_point;
                }elseif ($game->tiabreak == 1){
                    $tournament_table_1->points += $tournament_settings->lose_point_on_tiabreak;
                }else{
                    $tournament_table_1->points += $tournament_settings->lose_point;
                }
                $tournament_table_1->lose += 1;
                $tournament_table_1->increment('played_games');
            }elseif($game->winner == 0 and $game->winner !== NULL){
                $tournament_table_1->draw += 1;
                $tournament_table_1->increment('played_games');
            }

            $tournament_table_1->save();

            $tournament_table_2 = TournamentTable::where('participant_id', $game->player_2)->first();
            $tournament_table_2->gol += $game->result_2;
            $tournament_table_2->gol_against += $game->result_1;

            if ($game->winner == 2){
                if ($game->technical_lose == 1){
                    $tournament_table_2->points += $tournament_settings->technical_win_point;
                }elseif ($game->tiabreak == 1){
                    $tournament_table_2->points += $tournament_settings->win_point_on_tiabreak;
                }else{
                    $tournament_table_2->points += $tournament_settings->win_point;
                }
                $tournament_table_2->win += 1;
                $tournament_table_2->increment('played_games');
            }elseif($game->winner == 1){
                if ($game->technical_lose == 2 ){
                    $tournament_table_2->points += $tournament_settings->technical_lose_point;
                }elseif ($game->tiabreak == 1){
                    $tournament_table_2->points += $tournament_settings->lose_point_on_tiabreak;
                }else{
                    $tournament_table_2->points += $tournament_settings->lose_point;
                }
                $tournament_table_2->lose += 1;
                $tournament_table_2->increment('played_games');
            }elseif($game->winner == 0 and $game->winner !== NULL ){
                $tournament_table_2->draw += 1;
                $tournament_table_2->increment('played_games');
            }

            $tournament_table_2->save();

        }

        $tournament_table = TournamentTable::where('tournament_id',$tournament_id)->orderBy('points','desc')->get();
        $i=1;

        foreach ($tournament_table as $t){
            $t->place = $i++;

            $t->save();
        }

    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'result_1' => 'nullable|numeric|max:99',
            'result_2' => 'nullable|numeric|max:99',
//            'start_data' => 'date',
        ]);

        $game = Games::find($id);

        if ($request->get('result_1') > $request->get('result_2')) {
            $game->winner = 1;
        } elseif ($request->get('result_1') < $request->get('result_2')) {
            $game->winner = 2;
        } elseif($request->get('result_1') == $request->get('result_2')) {
            $game->winner = 0;
        }

        if ($request->tiabreak == 1){
            $game->tiabreak = 1;
        }


        if($request->technical_lose_1 == 1 ){
            $game->result_1 = 0;
            $game->result_2 = 3;
            $game->technical_lose = 1;
            $game->winner = 2;
        }elseif($request->technical_lose_2 == 2 ){
            $game->result_1 = 3;
            $game->result_2 = 0;
            $game->technical_lose = 2;
            $game->winner = 1;
        }else{
            $game->result_1 = $request->get('result_1');
            $game->result_2 = $request->get('result_2');
        }

        $game->start_data = $request->get('start_data');
        if ($request->get('end_data') != null) {
            $game->end_data = now();
        }

        $game->save();

        $this::initializeTable($game->tournament_id);

        $this::generateTable($game->tournament_id);

        return Redirect::back()->withErrors(['The game was successfully updated']);
//        return redirect()->back()->withErrors(['msg', 'The Message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Games $games
     * @return \Illuminate\Http\Response
     */
    public function destroy(Games $games)
    {
        //
    }
}
