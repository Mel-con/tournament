<?php

namespace App\Http\Controllers\Site;

use App\Games;
use App\Games2;
use App\Participant;
use App\Tournament;
use App\TournamentSettings;
use App\TournamentTable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;

class Games2Controller extends Controller
{
    public function store(Request $request , $id )
    {
        $tournament = Tournament::find($id);
        $games2 = Games2::where('tournament_id',$id)->where('round',$tournament->number_of_teams_shootout/2)->get();
        $p = Participant::where('tournament_id',$id)->update(['participant_status' => 0]);

        

        foreach ($games2 as $g){

            $games2 = Games2::find($g->id);
            $participants = Participant::where('tournament_id',$id)->get();

            if (($request->input('participant_1')[$g->id] !== '-') && ($g->round == $tournament->number_of_teams_shootout/2)){
                $g->player_1 = $request->input('participant_1')[$g->id];
                foreach ($participants as $participant ){
                    if ($participant->id == $request->input('participant_1')[$g->id] ){
                        $participant->participant_status = 1;
                    }
                    $participant->save();
                }

            }
            if (($request->input('participant_2')[$g->id] !== '-') && ($g->round == $tournament->number_of_teams_shootout/2)){
                $g->player_2 = $request->input('participant_2')[$g->id];
                foreach ($participants as $participant ) {
                    if ($participant->id == $request->input('participant_2')[$g->id]) {
                        $participant->participant_status = 1;
                    }
                    $participant->save();
                }
            }
            $g->save();
        }
        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'result_1' => 'nullable|numeric|max:99',
            'result_2' => 'nullable|numeric|max:99',
        ]);

        $games2 = Games2::find($id);

        if (strpos($games2->player_1,'A')!==false or strpos($games2->player_2,'A') !== false ){

            return redirect()->back()->withErrors(['First choice players, please']);
        }

        if ($request->get('result_1') > $request->get('result_2')) {
            $games2->winner = 1;
        } elseif ($request->get('result_1') < $request->get('result_2')) {
            $games2->winner = 2;
        } elseif($request->get('result_1') == $request->get('result_2')) {
            $games2->winner = 0;
        }

        if ($request->tiabreak == 1){
            $games2->tiabreak = 1;
        }


        if($request->technical_lose_1 == 1 ){
            $games2->result_1 = 0;
            $games2->result_2 = 3;
            $games2->technical_lose = 1;
            $games2->winner = 2;
        }elseif($request->technical_lose_2 == 2 ){
            $games2->result_1 = 3;
            $games2->result_2 = 0;
            $games2->technical_lose = 2;
            $games2->winner = 1;
        }else{
            $games2->result_1 = $request->get('result_1');
            $games2->result_2 = $request->get('result_2');
        }

        $games2->start_data = $request->get('start_data');
        if ($request->get('end_data') != null) {
            $games2->end_data = now();
        }

        $games2->save();

        $all_games2 = Games2::where('tournament_id',$games2->tournament_id)->get();



        foreach ($all_games2 as $all_g){
            if($all_g->round == $games2->round/2){

//                echo "<pre>";
//                print_r($all_g);
//                dd();
                if($games2->game_number/2 == $all_g->game_number){
                    if ($request->get('result_1')>$request->get('result_2')){
                        $all_g->player_2 = $games2->player_1;



                        $all_g->save();
                    }
                    elseif ($request->get('result_1')<$request->get('result_2')){
                        $all_g->player_2 = $games2->player_2;
                        $all_g->save();
                    }
                }elseif (round($games2->game_number/2) == $all_g->game_number){
                    if ($request->get('result_1')>$request->get('result_2')){
                        $all_g->player_1 = $games2->player_1;
                        $all_g->save();
                    }
                    elseif ($request->get('result_1')<$request->get('result_2')){
                        $all_g->player_1 = $games2->player_2;
                        $all_g->save();
                    }
                }
            }

        }



        return redirect()->back();
    }
}
