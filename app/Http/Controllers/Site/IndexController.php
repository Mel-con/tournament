<?php

namespace App\Http\Controllers\Site;

use App\index;
use App\Tournament;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $tournament = Tournament::paginate(10);
        return view('site/index',compact('tournament'));
    }
}
