<?php

namespace App\Http\Controllers\Site;


use App\Games;
use App\Games2;
use App\Participant;
use App\TournamentSettings;
use App\TournamentTable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Tournament;

class TournamentController extends Controller
{

    public function index()
    {
        return view("site/creat_tournament");
    }

    public static function several($tournament,$quantity){

        if ($tournament->number_of_teams_shootout>=$quantity){
            for ($i = 0 , $j=0 ; $i<$tournament->number_of_teams_shootout ; $i+=$quantity, $j++){
                $games2 = new Games2();
                $games2->tournament_id = $tournament->id;
                $games2->game_number = $j + 1;
                $games2->player_1 =  'A'.($i+1).'/A'.($i+2);
                $games2->player_2 = 'A'. ($i+3).'/A'.($i+4);
                $games2->round = $tournament->number_of_teams_shootout/$quantity;
                $games2-> save();
            }


        }
    }
    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required|max:30|min:2',
            'game_name' => 'required|max:30|min:2',
            'city' => 'required|max:50',
            'number_of_rounds' => 'nullable|numeric',
            'number_of_teams_shootout' => 'nullable|numeric',
            'third_place_match' => 'nullable|max:1',
            'number_of_teams_double' => 'nullable|numeric',
            'tournament_type' => 'required|numeric',
        ]);

        $tournament = new Tournament();
        $tournament->user_id = 1;
        $tournament->name = $request->input('name');
        $tournament->game_name = $request->input('game_name');
        $tournament->city = $request->input('city');
        $tournament->number_of_rounds = $request->input('number_of_rounds');
        $tournament->number_of_teams_shootout = $request->input('number_of_teams_shootout');
        $tournament->third_place_match = $request->input('third_place_match');
        $tournament->number_of_teams_double = $request->input('number_of_teams_double');
        $tournament->tournament_type = $request->input('tournament_type');
        $tournament->save();

        $tournament_settings = new TournamentSettings();

        $tournament_settings->tournament_id = $tournament->id;

        $tournament_settings->save();


        if ($tournament->tournament_type == 1 ) {
            return redirect('/tournament/show/' . $tournament->id);
        }elseif ($tournament->tournament_type == 2){

            for($i=2; $i<=$tournament->number_of_teams_shootout;$i*=2)
                TournamentController::several($tournament,$i);

            if ($tournament->third_place_match == 1){
                $games2 = new Games2();
                $games2->tournament_id = $tournament->id;
                $games2->game_number = 2;
                $games2->player_1 =  'A'.($i+1).'/A'.($i+2);
                $games2->player_2 = 'A'. ($i+3).'/A'.($i+4);
                $games2->round = '1/3';
                $games2-> save();
            }

            return redirect('/tournament/show/' . $tournament->id);


        } else {
            return view("site/index");
        }
    }

    public function show($id)
    {
        $tournament = Tournament::find($id);

        if($tournament->tournament_type == 1){
            $participants = Participant::where('tournament_id', $id)->get();
            $games = Games::leftjoin('participants as part1', 'part1.id', '=', 'games.player_1')
                ->leftjoin('participants as part2', 'part2.id', '=', 'games.player_2')
                ->select('games.*', 'part1.participant as participant_1', 'part2.participant as participant_2')
                ->where('games.tournament_id', $id)->get();

            $tournament_table = TournamentTable::
            leftjoin('participants', 'participants.id', '=', 'tournament_tables.participant_id')
                ->select('tournament_tables.*', 'participants.participant')
                ->where('tournament_tables.tournament_id', $id)->orderBy('place')->get();


            $tournament_settings = TournamentSettings::
            leftjoin('tournaments','tournaments.id','=','tournament_settings.tournament_id')
                ->select('tournament_settings.*', 'tournaments.name')
                ->where('tournament_settings.tournament_id', $id)->get();

            return view('site/tournament', compact('tournament', 'participants', 'games','tournament_table', 'tournament_settings'));
        }elseif ($tournament->tournament_type == 2){

            $games2 = Games2::leftjoin('participants as part1', 'part1.id', '=', 'games2.player_1')
                ->leftjoin('participants as part2', 'part2.id', '=', 'games2.player_2')
                ->select('games2.*', 'part1.participant as participant_1', 'part2.participant as participant_2')
                ->where('games2.tournament_id', $tournament->id)->get();
            ;
            $participants = Participant::where('tournament_id', $id)->get();
//
            return view('site/tournament2', compact('tournament','games2','participants'));
        }



    }


    public function update(Request $request)
    {
        //
    }

    public function delete($id)
    {
        Tournament::find($id)->delete();
        Games::where('tournament_id',$id)->delete();
        Games2::where('tournament_id',$id)->delete();
        TournamentTable::where('tournament_id',$id)->delete();
        TournamentSettings::where('tournament_id',$id)->delete();
        Participant::where('tournament_id',$id)->delete();

        return back()->with('success', 'team member deleted successfully');
    }
}
