<?php

namespace App\Http\Controllers\Site;

use App\Games;
use App\Participant;
use App\Tournament;
use App\TournamentTable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'participant' => 'required|max:30|min:2',
        ]);

        $participants = Participant::where('tournament_id',$request->input('tournament_id'))->get();
        $tournaments = Tournament::where('id', $request->input('tournament_id'))->first();

        $participant = new Participant();
        $participant->user_id = 1;
        $participant->tournament_id = $request->input('tournament_id');
        $participant->participant = $request->input('participant');
        $participant->save();

        $tournament_table = new TournamentTable();
        $tournament_table->tournament_id = $request->input('tournament_id');
        $tournament_table->participant_id = $participant->id;
        $tournament_table->save();

        foreach ($participants as $p){

            if($tournaments->number_of_rounds >=1){
                $game = new Games();
                $game->tournament_id = $request->input('tournament_id');
                $game->player_1 = $p->id;
                $game->player_2 = $participant->id;
                $game->save();
            }

            if($tournaments->number_of_rounds>=2){
                $game = new Games();
                $game->tournament_id = $request->input('tournament_id');
                $game->player_1 = $participant->id;
                $game->player_2 = $p->id;
                $game->save();
            }

            if($tournaments->number_of_rounds>=3){
                $game = new Games();
                $game->tournament_id = $request->input('tournament_id');
                $game->player_1 = $p->id;
                $game->player_2 = $participant->id;
                $game->save();
            }

            if($tournaments->number_of_rounds>=4){
                $game = new Games();
                $game->tournament_id = $request->input('tournament_id');
                $game->player_1 = $participant->id;
                $game->player_2 = $p->id;
                $game->save();

            }
        }
        return redirect('/tournament/show/'.$participant->tournament_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Participant  $participant
     * @return \Illuminate\Http\Response
     */
    public function show(ParticipantAlias $participant)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'participant' => 'required|max:30|min:2',
        ]);

        $participant = Participant::find($id);
        $participant->participant = $request->input('participant');
        $participant->save();

        return back()->with('success', 'team member deleted successfully');
    }


    public function delete($id)
    {
        Participant::find($id)->delete();
        Games::where('player_1',$id)->delete();
        Games::where('player_2',$id)->delete();
        TournamentTable::where('participant_id',$id)->delete();

        return back()->with('success', 'team member deleted successfully');
    }
}
