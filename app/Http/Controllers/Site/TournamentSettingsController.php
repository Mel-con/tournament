<?php

namespace App\Http\Controllers\Site;

use App\Tournament;
use App\TournamentSettings;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;

class TournamentSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TournamentSettings  $tournamentSettings
     * @return \Illuminate\Http\Response
     */
    public function show(TournamentSettings $tournamentSettings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TournamentSettings  $tournamentSettings
     * @return \Illuminate\Http\Response
     */
    public function edit(TournamentSettings $tournamentSettings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TournamentSettings  $tournamentSettings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:99|min:1',
            'description' => 'max:255',
            'win_point' => 'numeric|max:9|min:-9',
            'draw_point' => 'numeric|max:9|min:-9',
            'lose_point' => 'numeric|max:9|min:-9',
            'win_point_on_tiabreak' => 'numeric|max:9|min:-9',
            'lose_point_on_tiabreak' => 'numeric|max:9|min:-9',
            'technical_win_point' => 'numeric|max:9|min:-9',
            'technical_lose_point' => 'numeric|max:9|min:-9',
        ]);

        $tournament_settings = TournamentSettings::find($id);

        $tournament = Tournament::where('id' ,$tournament_settings->tournament_id)->first();
        $tournament->name = $request->get('name');
        $tournament->save();

        $tournament_settings->description = $request->get('description');
        $tournament_settings->win_point = $request->get('win_point');
        $tournament_settings->draw_point = $request->get('draw_point');
        $tournament_settings->lose_point = $request->get('lose_point');
        $tournament_settings->win_point_on_tiabreak = $request->get('win_point_on_tiabreak');
        $tournament_settings->lose_point_on_tiabreak = $request->get('lose_point_on_tiabreak');
        $tournament_settings->technical_win_point = $request->get('technical_win_point');
        $tournament_settings->technical_lose_point = $request->get('technical_lose_point');

//        echo '<pre>';
//        print_r($tournament_settings);
//        print_r($request->all());
//        die();

        $tournament_settings->save();


        return Redirect::back()->withErrors(['The game settings was successfully updated']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TournamentSettings  $tournamentSettings
     * @return \Illuminate\Http\Response
     */
    public function destroy(TournamentSettings $tournamentSettings)
    {
        //
    }
}
