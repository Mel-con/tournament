<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentSettings extends Model
{
    protected $table = "tournament_settings";
}
