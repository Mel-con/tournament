<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TournamentTable extends Model
{
    protected $table = "tournament_tables";
}
