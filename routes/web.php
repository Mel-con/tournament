<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login/{provider}/redirect',            'Auth\SocialController@social_redirect');
Route::get('/login/{provider}/callback',            'Auth\SocialController@social_callback');
Route::get('/logout',                               'Auth\SocialController@signout');

Route::get('/' ,                                    'Site\IndexController@index');

Route::get ('/tournament/create',                   'Site\TournamentController@index');
Route::post('/tournament/create',                   'Site\TournamentController@create');
Route::get ('/tournament/show/{id}',                'Site\TournamentController@show');
Route::get ('/tournament/delete/{id}',                'Site\TournamentController@delete');

Route::post('/tournament/participant/create',       'Site\ParticipantController@store');
Route::get ('/tournament/participant/delete/{id}',  'Site\ParticipantController@delete');
Route::post('/tournament/participant/update/{id}',  'Site\ParticipantController@update');
Route::post('/tournament/game/update/{id}',         'Site\GamesController@update');

Route::post('/tournament/settings/update/{id}',  'Site\TournamentSettingsController@update');

Route::post('/tournament/game2/store/{id}',         'Site\Games2Controller@store');
Route::post('/tournament/game2/update/{id}',         'Site\Games2Controller@update');


