

<div class="container header-top-mobile" style="clear: both">
    <div class="header-bars" style=""><span class="fa fa-bars"></span></div>
    <div class="header-mobile-logo" style=""><a href="/"><p>PES Tournaments</p></a></div>
</div>

<div class="mobile-menu">
    <button type="button" class="close close-menu">×</button>
    <div class="mobile-menu-item"><a href="/tournament-types">TOURNAMENT TYPES</a></div>
    <div class="mobile-menu-item"><a href="/about-us">ABOUT US</a></div>
    <div class="mobile-menu-item"><a href="/contact-us">CONTACT US</a></div>
    <div class="mobile-menu-item">
        @if(Auth::user())
            {{Auth::user()->first_name}} <a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
        @else
            <a href="#" id="mobile-sign_in_btn" data-toggle="modal" data-target="#sign-in">SIGN IN</a>
        @endif
    </div>
    <div class="mobile-menu-item">
        <div class="header-follow-us" style="margin-top: 0">
            <div class="instagram-link">
                <a href="http://www.instagram.com" title="instagram" target="_blank"><span class="instagram"></span></a>
            </div>
            <div class="facebook-link">
                <a href="http://www.facebook.com" title="Facebook" target="_blank"><span class="facebook"></span></a>
            </div>
        </div>
    </div>
</div>

<div style="clear: both"></div>

<div class="header-top-desktop">
    <div class="header-mobile-logo" style=""><a href="/"><p>PES Tournaments</p></a></div>
</div>

<div class="menu">
    <div class="menu-item"><a href="/tournament-types">TOURNAMENT TYPES</a></div>
    <div class="menu-item"><a href="/about-us">ABOUT US</a></div>
    <div class="menu-item"><a href="/contact-us">CONTACT US</a></div>
    <div class="header-menu-desktop-visible">
        <div class="menu-cart-item">

        </div>
        <div class="menu-item">
            @if(Auth::user())
                {{Auth::user()->first_name}} <a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
            @else
                <a href="#" data-toggle="modal" data-target="#sign-in">SIGN IN</a>
            @endif
        </div>
    </div>
    <div class="header-follow-us">
        <div class="instagram-link">
            <a href="http://www.instagram.com" title="instagram" target="_blank"><span class="instagram"></span></a>
        </div>
        <div class="facebook-link">
            <a href="http://www.facebook.com" title="Facebook" target="_blank"><span class="facebook"></span></a>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="sign-in" role="dialog" style="margin-top:150px">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal" style="outline: 0;">&times;</button>
            </div>
            <div class="modal-body" style="padding:20px">

                <center>
                    <br />
                    <div class="btn-group">
                        <a class='btn btn-danger disabled'href="/login/Google/redirect"><i class="fa fa-google-plus" style="width:16px; height:20px"></i></a>
                        <a class='btn btn-danger' href="/login/Google/redirect" style="width:14em;"> Sign in with Google</a>
                    </div>
                    <br /><br />
                    <div class="btn-group">
                        <a class='btn btn-primary disabled' href="/login/Facebook/redirect"><i class="fa fa-facebook" style="width:16px; height:20px"></i></a>
                        <a class='btn btn-primary ' href="/login/Facebook/redirect" style="width:14em"> Sign in with Facebook</a>
                    </div>
                    <br /><br />
                </center>



            </div>
        </div>

    </div>
</div>
