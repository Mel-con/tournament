@extends('site.layout')
@section('content')
    <div class="row">
        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        @if($errors->any())
            {{ implode('', $errors->all('message')) }}
        @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


        <form action="" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">1.Tournament name</label>
                <input type="text" name="name" class="form-control"  aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">2.Tournament game name</label>
                <input list="encodings" name="game_name" value="" class="form-control"  aria-describedby="">
                <datalist  id="encodings">
                    <option value="PES">PES</option>
                    <option value="Mortal Combat">Mortal Combat</option>
                    <option value="FIFA">FIFA</option>
                </datalist>
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">3.Tournament city</label>
                <input type="text" name="city" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">4.Tournament type</label>
            </div>
            <div id="accordion">
                <div class="card ">
                    <div class="card-header" data-header="1" id="headingOne">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Round tournament
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body ">
                            <label for="exampleInputEmail1">Number of rounds</label>
                            <select list="encodings" name="number_of_rounds" value="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                <option value="" selected="">please select number</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                            </select>
                           <h5> The most common round robin tournament where everyone plays with everyone.</h5>
                            Default:
                            <p>victory - <b>3 points</b>
                            <p>tiebreak victory - <b>2 points</b>
                            <p>tiebreak loss - <b>1 point</b>
                            <p>for a draw - <b>1 point</b>
                            <p>for a technical victory - <b>3 points</b>
                            <p>for technical defeat - <b>0 points</b>
                            <p><h5><i>You can change the number of points at any time in the "Settings" tab.</i></h5>


                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" data-header="2" id="headingTwo">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Shootout Tournament
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">

                           <p>Tournament in the Olympic system, the losing team is eliminated.</p>

                            <label for="exampleInputEmail1">Number of teams:</label>
                            <select list="encodings" name="number_of_teams_shootout" value="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                <option value="" selected="">please select number</option>
                                <option value="4" >4</option>
                                <option value="8">8</option>
                                <option value="16">16</option>
                                <option value="32">32</option>
                            </select>

                            <label for="exampleInputEmail1">Third place match:</label>
                            <select list="encodings" name="third_place_match" value="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" data-header="3" id="headingThree">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Double elimination
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                         <p>A double-elimination tournament is a type of elimination tournament competition</p>
                            <p>which a participant ceases to be eligible to win the tournament's championship</p>
                            <p>upon having lost two games or matches. It stands in contrast to a single-elimination </p>
                            <p>tournament, in which only one defeat results in elimination.</p>

                            <label for="exampleInputEmail1">Number of teams:</label>
                            <select list="encodings" name="number_of_teams_double" value="" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" >
                                <option value="" selected="">please select number</option>
                                <option value="4" >3-4</option>
                                <option value="8">5-8</option>
                                <option value="16">9-16</option>
                                <option value="32">17-32</option>
                                <option value="32">33-64</option>
                            </select>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" data-header="4" id="headingFour">
                        <h5 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Common tournament
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            <p>Common tournament designed to display growing growth results in each round.</p>
                             <p>   Used when the number of points scored in each round may vary.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">5.Go to editing</label>
            </div>
            <input id="tournament_type" name="tournament_type" type="hidden" >
            <button class="btn btn-primary btn-lg btn-block" type="submit">OK</button>

        </form>
    </div>

@stop
