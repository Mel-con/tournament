@extends('site.layout')
@section('content')

    <div class="row">
        <div class="col-md-offset-2 col-md-3">
            <a  href="/tournament/create" type="button" class="btn btn-primary btn-lg btn-block">Create new tournament </a>
        </div>
    </div>

    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User</th>
                <th scope="col">Name</th>
                <th scope="col">Game</th>
                <th scope="col">City</th>
                <th scope="col">Type</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>

            @foreach($tournament as $t)
            <tr>
                <th scope="row">{{$t->id}}</th>
                <td>{{$t->user_id}}</td>
                <td>{{$t->name}}</td>
                <td>{{$t->game_name}}</td>
                <td>{{$t->city}}</td>
                <td>{{$t->tournament_type}}</td>
                <td><a href="/tournament/show/{{$t->id}}"><i class="fa fa-eye"></i></a></td>
                <td><a href="/tournament/delete/{{$t->id}}"><i class="fa fa-x">x</i></a></td>
            </tr>
            @endforeach

            </tbody>
        </table>



    </div>







@stop
