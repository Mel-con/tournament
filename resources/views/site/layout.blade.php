<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Tournament</title>

    <!-- css only -->

    <link rel="stylesheet" href="/bootstrap4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/css/pstyle.css" >
{{--    <link rel="stylesheet/less" type="text/css" href="/css/style.less" >--}}
    <link rel="stylesheet" href="/css/custom.css" >

    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="/bootstrap4/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700,800,900&display=swap" rel="stylesheet">

    <!-- font awesome css -->
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>

<body>

@include('site.header')

<div class="container ">

    @yield('content')

<!-- Footer -->
    <footer class="page-footer font-small mdb-color pt-4">

        <!-- Footer Links -->
        <div class="container text-center text-md-left">

            <!-- Footer links -->
            <div class="row text-center text-md-left mt-3 pb-3">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Company name</h6>
                    <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
                        consectetur
                        adipisicing elit.</p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Products</h6>
                    <p>
                        <a href="#!">MDBootstrap</a>
                    </p>
                    <p>
                        <a href="#!">MDWordPress</a>
                    </p>
                    <p>
                        <a href="#!">BrandFlow</a>
                    </p>
                    <p>
                        <a href="#!">Bootstrap Angular</a>
                    </p>
                </div>
                <!-- Grid column -->

                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Useful links</h6>
                    <p>
                        <a href="#!">Your Account</a>
                    </p>
                    <p>
                        <a href="#!">Become an Affiliate</a>
                    </p>
                    <p>
                        <a href="#!">Shipping Rates</a>
                    </p>
                    <p>
                        <a href="#!">Help</a>
                    </p>
                </div>

                <!-- Grid column -->
                <hr class="w-100 clearfix d-md-none">

                <!-- Grid column -->
                <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                    <p>
                        <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>
                    <p>
                        <i class="fas fa-envelope mr-3"></i> info@gmail.com</p>
                    <p>
                        <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>
                    <p>
                        <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Footer links -->

            <hr>

            <!-- Grid row -->
            <div class="row d-flex align-items-center">

                <!-- Grid column -->
                <div class="col-md-7 col-lg-8">

                    <!--Copyright-->
                    <p class="text-center text-md-left">© 2020 Copyright:
                        <a href="https://mdbootstrap.com/">
                            <strong> MDBootstrap.com</strong>
                        </a>
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-5 col-lg-4 ml-lg-0">

                    <!-- Social buttons -->
                    <div class="text-center text-md-right">
                        <ul class="list-unstyled list-inline">
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-google-plus-g"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a class="btn-floating btn-sm rgba-white-slight mx-1">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

    </footer>
    <!-- Footer -->

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<!-- Modal -->
<div class="modal fade" id="sign-in" role="dialog" style="margin-top:150px">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Sign In</h4>
                <button type="button" class="close" data-dismiss="modal" style="outline: 0;">&times;</button>
            </div>
            <div class="modal-body" style="padding:20px">

                <center>
                    <br />
                    <div class="btn-group">
                        <a class='btn btn-danger disabled'href="/login/Google/redirect"><i class="fa fa-google-plus" style="width:16px; height:20px"></i></a>
                        <a class='btn btn-danger' href="/login/Google/redirect" style="width:14em;"> Sign in with Google</a>
                    </div>
                    <br /><br />
                    <div class="btn-group">
                        <a class='btn btn-primary disabled' href="/login/Facebook/redirect"><i class="fa fa-facebook" style="width:16px; height:20px"></i></a>
                        <a class='btn btn-primary ' href="/login/Facebook/redirect" style="width:14em"> Sign in with Facebook</a>
                    </div>
                    <br /><br />
                </center>



            </div>
        </div>

    </div>
</div>

<script>
    $(document).on('click', '.fa-bars', function(){
        $('.mobile-menu').toggle();
    });
    $(document).on('click', '.close-menu', function(){
        $('.mobile-menu').toggle();
    });
    $(document).on('click', '#mobile-sign_in_btn', function(){
        $('.mobile-menu').toggle();
    });
</script>

<script>
    $(document).ready(function(){
        $(".card-header").click(function(){
            $("#tournament_type").val($(this).data('header'));
        });
    });
</script>


<script>
    $(".participant_edit").click(function () {
        var id = $(this).data('id');
        // console.log(id);
        $("#participant_edit_1_"+id).hide();
        $("#participant_edit_input_"+id).show();
    });

    $(".participant_edit_cancel").click(function () {
        var id = $(this).data('id');
        // console.log(id);
        $("#participant_edit_1_"+id).show();
        $("#participant_edit_input_"+id).hide();
    });

</script>

<script>

    $("#Participants-tab").click(function Modal() {
        localStorage.setItem('mymodal', 'Participants-tab');
    });
    $("#Overview-tab").click(function Modal() {
        localStorage.setItem('mymodal', 'Overview-tab');
    });
    $("#Games-tab").click(function Modal() {
        localStorage.setItem('mymodal', 'Games-tab');
    });
    $("#Settings-tab").click(function Modal() {
        localStorage.setItem('mymodal', 'Settings-tab');
    });

    $( document ).ready(function() {
        if(localStorage.getItem('mymodal')=='Overview-tab') {
            console.log("Hello world!");
//            $(".nav-link").removeClass('active');
//            $(".tab-pane").removeClass('show');
//            $(".nav-link").attr("aria-selected","false");
//            $("#Overview-tab").addClass('active');
//            $("#Overview-tab").addClass('show');
//            $("#Overview-tab").attr("aria-selected","true");

            $('.nav-tabs a[href="#Overview"]').tab('show');
        }
    });

</script>




<script src="http://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
</script>
<script>
    jQuery(document).ready(function(){
        jQuery('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "{{ url('/grocery/post') }}",
                method: 'post',
                data: {
                    name: jQuery('#name').val(),
                    type: jQuery('#email').val(),
                    price: jQuery('#address').val()
                },
                success: function(result){
                    console.log(result);
                }});
        });
    });
</script>


</body>
</html>
