<div class="tournament_2">
    <h1>Tournament Bracket</h1>
    <h2> Men's tournament</h2>

    <div class="tournament-bracket">
        @for($i = $tournament->number_of_teams_shootout/2 ; $i >= 1 ; $i/=2 )
            @if($tournament->number_of_teams_shootout/2 >= $i)
                <div class="tournament-bracket">
                    <div class="round_name tournament-bracket-item ">1/{{$i}}</div>
                </div>
            @endif
        @endfor
    </div>


    <div class="tournament-bracket">
        @for($i = $tournament->number_of_teams_shootout/2 ; $i >= 1 ; $i/=2 )

        @if($tournament->number_of_teams_shootout/2 >= $i)

        <div class="tournament-bracket-item" >

                    <form method="post" action="/tournament/game2/store/{{$tournament->id}}" enctype="multipart/form-data">
                        @csrf
                        @foreach($games2 as $g)
                            @if($g->round == $i )
                                <div class="items">
                                    <div class="col-md-12">
                                        dd;mm;yyyy
                                    </div>

                                    <div class="col-md-12 part1">
                                        <div class="col-md-5"> <span @if($g->result_1 > $g->result_2) class="winner" @endif >@if($g->result_1 !== Null){{$g->result_1}}@else - @endif</span></div>
                                        <div class="col-md-2">:</div>
                                        <div class="col-md-5"><span @if($g->result_1 < $g->result_2) class="winner" @endif>@if($g->result_2 !== Null){{$g->result_2}}@else - @endif</span></div>
                                    </div>
                                    <div class="col-md-12 part2">
                                        <div class="col-md-6"> @if($g->round == $tournament->number_of_teams_shootout/2)
                                                <select name="participant_1[{{$g->id}}]"  id="participant_1[{{$g->id}}]">
                                                    <option value="@if(strpos($g->player_1,'A') !== false) - @else {{$g->player_1}} @endif"> @if(strpos($g->player_1,'A') !== false)
                                                            - @else {{$g->participant_1}} @endif </option>
                                                    @foreach($participants as $p)
                                                        @if($p->participant_status !== 1  )

                                                            <option value="{{$p->id}}">{{$p->participant}}</option>

                                                        @endif
                                                    @endforeach
                                                </select>
                                            @elseif(strpos($g->player_1,'A') !== false)
                                                <p> Team </p> @else <p>{{$g->participant_1}}</p> @endif
                                        </div>
                                        <div class="col-md-6">@if($g->round == $tournament->number_of_teams_shootout/2)

                                                <select name="participant_2[{{$g->id}}]" id="participant_2[{{$g->id}}]">
                                                    <option value="@if(strpos($g->player_2,'A') !== false) -  @else {{$g->player_2}} @endif"> @if(strpos($g->player_2,'A') !== false) -  @else {{$g->participant_2}} @endif </option>
                                                    @foreach($participants as $p)
                                                        @if($p->participant_status !== 1  )

                                                            <option value="{{$p->id}}">{{$p->participant}}</option>

                                                        @endif
                                                    @endforeach
                                                </select>
                                            @elseif(strpos($g->player_2,'A') !== false) <p> Team </p>  @else <p>{{$g->participant_2}}</p> @endif
                                        </div>
                                    </div>

                                </div>


                                {{--<div class="items">--}}
                                {{--<div class="items-content">--}}
                                {{--<div>@if($g->round == $tournament->number_of_teams_shootout/2)--}}
                                {{--<select name="participant_1[{{$g->id}}]" >--}}
                                {{--<option value="@if(strpos($g->player_1,'A') !== false) - @else {{$g->player_1}} @endif"> @if(strpos($g->player_1,'A') !== false) - @else {{$g->participant_1}} @endif </option>--}}
                                {{--@foreach($participants as $p)--}}
                                {{--<option value="{{$p->id}}">{{$p->participant}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif(strpos($g->player_1,'A') !== false) ?? @else {{$g->participant_1}} @endif  @if($g->result_1 !== Null){{$g->result_1}}@else - @endif--}}
                                {{--</div>--}}
                                {{--<div>@if($g->round == $tournament->number_of_teams_shootout/2)--}}

                                {{--<select name="participant_2[{{$g->id}}]">--}}
                                {{--<option value="@if(strpos($g->player_2,'A') !== false) -  @else {{$g->player_2}} @endif"> @if(strpos($g->player_2,'A') !== false) -  @else {{$g->participant_2}} @endif </option>--}}
                                {{--@foreach($participants as $p)--}}
                                {{--<option value="{{$p->id}}">{{$p->participant}}</option>--}}
                                {{--@endforeach--}}
                                {{--</select>--}}
                                {{--@elseif(strpos($g->player_2,'A') !== false) ??  @else {{$g->participant_2}} @endif  @if($g->result_2 !== Null){{$g->result_2}}@else - @endif</div>--}}
                                {{--</div>--}}

                    {{--</div>--}}
                @endif
            @endforeach
                <button type="submit" @if($i  !== $tournament->number_of_teams_shootout/2)style="display: none" @endif>save</button>
        </form>
        </div>
        @endif
        @endfor
    </div>
</div>

