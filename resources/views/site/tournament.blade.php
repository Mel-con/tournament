@extends('site.layout')
@section('content')


    <div class="col-md-3 offset-5">
        <h2> {{$tournament->name}}</h2>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="Participants-tab" data-toggle="tab" href="#Participants" role="tab"
               aria-controls="Participants" aria-selected="true">Participants</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="Overview-tab" data-toggle="tab" href="#Overview" role="tab" aria-controls="Overview"
               aria-selected="false">Overview</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="Games-tab" data-toggle="tab" href="#Games" role="tab" aria-controls="Games"
               aria-selected="false">Games</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="Settings-tab" data-toggle="tab" href="#Settings" role="tab" aria-controls="Settings"
               aria-selected="false">Settings</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="Participants" role="tabpanel" aria-labelledby="Participants-tab">
            <div class="col-12 participant-content">
                <div class=" col-12 participant-content">
                    <form action="/tournament/participant/create" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="input-group ">
                            <input type="text" name="participant" class="form-control" placeholder="Enter a participant name"
                                   aria-label="Enter a participant name" aria-describedby="basic-addon2">
                            <input type="hidden" name="tournament_id" value="{{$tournament->id}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">ADD</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-12 participant-content-items">
                    @foreach($participants as $part)
                        <div class="participant-content-item" id="participant_edit_1_{{$part->id}}">
                            {{$part->participant}}
                            <a>
                                <i class="fa fa-pencil-square-o participant_edit" data-id="{{$part->id}}" id="participant_edit" aria-hidden="true"></i>
                            </a>
                            <a href="/tournament/participant/delete/{{$part->id}}">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                        </div>
                                <form method="post" action="/tournament/participant/update/{{$part->id}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="input-group participant-content-input-edit " id="participant_edit_input_{{$part->id}}"
                                         style="display: none">
                                        <input type="" value="{{$part->participant}}" onkeypress="this.style.width = ((this.value.length + 1) * 8) + 'px';" name="participant" class=" " aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="participant_edit_ok" type="submit">ok</button>
                                            <button class="participant_edit_cancel"
                                                    data-id="{{$part->id}}" type="button">x
                                            </button>
                                        </div>
                                    </div>
                                </form>
                    @endforeach
                </div>
            </div>


        </div>
        <div class="tab-pane fade tournament-table" id="Overview" role="tabpanel" aria-labelledby="Overview-tab">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Pos</th>
                    <th scope="col">Team</th>
                    <th scope="col">Played</th>
                    <th scope="col">Wins</th>
                    <th scope="col">Draws</th>
                    <th scope="col">Losses</th>
                    <th scope="col">GF</th>
                    <th scope="col">GA</th>
                    <th scope="col">GD</th>
                    <th scope="col">Points</th>
                </tr>
                </thead>
                <tbody>

                @foreach($tournament_table as $t)

                    <tr>
                        <td>@if($t->place!==null){{$t->place}}@else - @endif</td>
                        <td><img
                                src="https://icons.iconarchive.com/icons/giannis-zographos/spanish-football-club/256/Real-Madrid-icon.png"> @if($t->participant!==null){{$t->participant}}@else
                                - @endif </td>
                        <td>@if($t->played_games!==null){{$t->played_games}}@else - @endif </td>
                        <td>@if($t->win!==null){{$t->win}}@else - @endif</td>
                        <td>@if($t->draw!==null){{$t->draw}}@else - @endif</td>
                        <td>@if($t->lose!==null){{$t->lose}}@else - @endif</td>
                        <td>@if($t->gol!==null){{$t->gol}}@else - @endif</td>
                        <td>@if($t->gol_against!==null){{$t->gol_against}}@else - @endif</td>
                        <td>@if($t->gol - $t->gol_against !==null){{$t->gol - $t->gol_against}}@else - @endif</td>
                        <td>@if($t->points!==null){{$t->points}}@else - @endif</td>
                    </tr>

                @endforeach
                </tbody>
            </table>


            <table class="table">
                <tbody>
                <tr>
                    <th>
                        participant
                    </th>
                @for($i = 0 ; $i<count($tournament_table) ; $i++)
                        <th>
{{--                            @if(isset($tournament_table[$i]))--}}
                                {{$tournament_table[$i]->participant}}
{{--                            @endif--}}
                        </th>
                @endfor
                    <th>points</th>
                    <th>place</th>
                </tr>
                @for($i = 0 ; $i<count($tournament_table) ; $i++)

                    <tr>
                        <th>

                                {{$tournament_table[$i]->participant}}

                        </th>

                        @for($m = 0 ; $m<count($tournament_table) ; $m++)
                        <td @if($tournament_table[$i]==$tournament_table[$m]) style="background-color: #3f74ad" @endif >
                            @if($tournament_table[$i]!=$tournament_table[$m])
                                @foreach($games as $g)
                                    @if($tournament_table[$i]->participant_id == $g->player_1 and $tournament_table[$m]->participant_id == $g->player_2 )
                                       <div>{{$g->result_1}} - {{$g->result_2}}</div>
                                    @endif
                                        @if($tournament_table[$i]->participant_id == $g->player_2 and $tournament_table[$m]->participant_id == $g->player_1 )
                                            <div>{{$g->result_2}} - {{$g->result_1}}</div>
                                        @endif
                                @endforeach
                            @endif
                        </td>
                        @endfor


                        <td>@if($tournament_table[$i]->points!==null){{$tournament_table[$i]->points}}@else - @endif</td>
                        <td>@if($tournament_table[$i]->place!==null){{$tournament_table[$i]->place}}@else - @endif</td>

                    </tr>
                @endfor
                </tbody>
            </table>



        </div>


        <div class="tab-pane fade" id="Games" role="tabpanel" aria-labelledby="games-tab">
            <div class="row games">
                @foreach($games as $g)
                    <div class="flex-container col-12">
                        <div style="flex: 3">3</div>
                        <div style="flex: 2">{{$g->participant_1}}</div>
                        <div style="flex: 1"><img
                                src="https://icons.iconarchive.com/icons/giannis-zographos/spanish-football-club/256/Real-Madrid-icon.png">
                        </div>
                        <div style="flex: 1"><b>{{$g->result_1}}</b></div>
                        <div style="flex: 1">FT</div>
                        <div style="flex: 1"><b>{{$g->result_2}}</b></div>
                        <div style="flex: 1"><img
                                src="https://icons.iconarchive.com/icons/giannis-zographos/spanish-football-club/256/Real-Madrid-icon.png">
                        </div>
                        <div style="flex: 2">{{$g->participant_2}}</div>
                        <div style="flex: 3">
                            <button type="button" class="btn " data-toggle="modal"
                                    data-target="#exampleModalCenter{{$g->id}}">Edit
                            </button>
                        </div>
                    </div>

                    <div class="modal fade" id="exampleModalCenter{{$g->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-top" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Edit game</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="/tournament/game/update/{{$g->id}}"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col  text-center justify-content-center form-inline">
                                                <div class="col-md-12">{{$g->participant_1}}</div>
                                                <div class="col-md-12"><input
                                                        value="@if($g->result_1!==null){{$g->result_1}}@endif"
                                                        type="text" name="result_1" class="form-control input-lg col-5">
                                                </div>

                                            </div>
                                            <div class="col  text-center justify-content-center form-inline">
                                                <div class="col-md-12">Tiebreak</div>
                                                <div class="col-md-12"><input type="checkbox"
                                                                              @if($g->tiabreak==1) checked
                                                                              @endif name="tiabreak" value="1"
                                                                              class="input-lg col-2">
                                                </div>

                                            </div>
                                            <div class="col text-center justify-content-center form-inline">
                                                <div class="col-md-12">{{$g->participant_2}}</div>
                                                <div class="col-md-12"><input
                                                        value="@if($g->result_2!==null){{$g->result_2}}@endif"
                                                        name="result_2" class="form-control input-lg col-5">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col  text-center justify-content-center form-inline">
                                                <div class="col-md-12">
                                                    <label for="exampleInputEmail1">Start of match</label>
                                                    <input type="datetime-local" value="{{$g->start_data}}"
                                                           name="start_data" class="form-control input-sm col-6">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="exampleInputEmail1">This game was end</label>
                                                    <input type="checkbox" name="end_data"
                                                           class="form-control input-sm col-6">
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col text-center justify-content-center form-inline ">
                                                <div class="col-md-4"><input type="checkbox" name="technical_lose_1"
                                                                             @if($g->technical_lose==1) checked
                                                                             @endif value="1" class="input-lg col-2">
                                                </div>
                                                <div class="col-md-4">Technical lose</div>
                                                <div class="col-md-4"><input type="checkbox" name="technical_lose_2"
                                                                             @if($g->technical_lose==2) checked
                                                                             @endif value="2" class="input-lg col-2">
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="tab-pane fade settings" id="Settings" role="tabpanel" aria-labelledby="Settings-tab">

            <div class="row">
                @foreach($tournament_settings as $settings)
                    <form action="/tournament/settings/update/{{$settings->id}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">

                            <label for="exampleInputEmail1">Tournament name</label>
                            <input type="text" value="{{$settings->name}}" name="name" class="form-control"
                                   aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Description</label>
                            <textarea class="form-control" name="description" id="exampleFormControlTextarea1"
                                      rows="4">{{$settings->description}}</textarea>
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Win points</label>
                            <input type="number" step="any" value="{{$settings->win_point}}" name="win_point"
                                   class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Draw points</label>
                            <input type="number" step="any" value="{{$settings->draw_point}}" name="draw_point"
                                   class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Lose points</label>
                            <input type="number" step="any" value="{{$settings->lose_point}}" name="lose_point"
                                   class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Win points on tiebreak</label>
                            <input type="number" step="any" value="{{$settings->win_point_on_tiabreak}}"
                                   name="win_point_on_tiabreak" class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Lose points on tiebreak</label>
                            <input type="number" step="any" value="{{$settings->lose_point_on_tiabreak}}"
                                   name="lose_point_on_tiabreak" class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>


                            <label for="exampleInputEmail1">Technical win points</label>
                            <input type="number" step="any" value="{{$settings->technical_win_point}}"
                                   name="technical_win_point" class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <label for="exampleInputEmail1">Technical lose points</label>
                            <input type="number" step="any" value="{{$settings->technical_lose_point}}"
                                   name="technical_lose_point" class="form-control" aria-describedby="emailHelp">
                            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                                else.</small>

                            <button class="btn btn-primary btn-lg btn-block" type="submit">Save</button>

                        </div>
                    </form>
                @endforeach
            </div>

        </div>
    </div>
@stop
