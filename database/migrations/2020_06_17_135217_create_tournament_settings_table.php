<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_settings', function (Blueprint $table) {
            $table->id();
            $table->string('tournament_id');
            $table->string('description')->nullable();
            $table->integer('win_point')->default(3);
            $table->integer('draw_point')->default(1);
            $table->integer('lose_point')->default(0);
            $table->integer('win_point_on_tiabreak')->default(2);
            $table->integer('lose_point_on_tiabreak')->default(1);
            $table->integer('technical_win_point')->default(3);
            $table->integer('technical_lose_point')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_settings');
    }
}
