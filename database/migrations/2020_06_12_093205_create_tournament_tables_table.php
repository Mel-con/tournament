<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournament_tables', function (Blueprint $table) {
            $table->id();
            $table->string('tournament_id')->nullable();
            $table->string('participant_id')->nullable();
            $table->string('played_games')->default(0);
            $table->string('win')->default(0);
            $table->string('lose')->default(0);
            $table->string('draw')->default(0);
            $table->string('gol')->default(0);
            $table->string('gol_against')->default(0);
            $table->string('points')->default(0);
            $table->string('place')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournament_tables');
    }
}
