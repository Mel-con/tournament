<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('tournament_id')->nullable();
            $table->string('player_1')->nullable();
            $table->string('player_2')->nullable();
            $table->string('result_1')->nullable();
            $table->string('result_2')->nullable();
            $table->string('start_data')->nullable();
            $table->string('end_data')->nullable();
            $table->string('winner')->nullable();
            $table->string('tiabreak')->nullable();
            $table->string('technical_lose')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
