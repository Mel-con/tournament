<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTournamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tournaments', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('game_name')->nullable();
            $table->string('city')->nullable();
            $table->string('number_of_rounds')->nullable();
            $table->string('number_of_teams_shootout')->nullable();
            $table->string('third_place_match')->nullable();
            $table->string('number_of_teams_double')->nullable();
            $table->string('tournament_type')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tournaments');
    }
}
